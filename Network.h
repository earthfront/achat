#pragma once

#include "Message.h"

#include <boost/asio.hpp>
#include <boost/asio/read.hpp>
#include <boost/system/error_code.hpp>

#include <memory>
#include <iostream>

#include <cassert>

using namespace std;
using namespace boost;
using namespace asio::ip;

namespace achat {
    Message::Header headerFromBytes(ByteVec& data){
        Message::Header result;
        auto size = Message::HeaderSize;
        assert(size == data.size());
        memcpy(&result, &data[0], size);
        return result;
    }

    void bytesFromHeader(ByteVec& bytes, Message::Header& header){
        auto size = Message::HeaderSize;
        bytes.resize(size);
        memcpy(&bytes[0], &header, size);
    }
}
