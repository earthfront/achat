#include "Message.h"
#include "ChatService.h"

#include <boost/date_time/local_time/local_time.hpp>
#include <boost/lexical_cast.hpp>

#include <iostream>
#include <future>
#include <atomic>
#include <memory>
#include <string>
#include <cstdint>
#include <vector>

using namespace std;
using namespace achat;

constexpr auto DEFAULT_ADDRESS = "localhost";
constexpr auto DEFAULT_PORT = "18300";

void printHelp(){
    cout << "Usage: achat -s [<port>]" << endl;
    cout << "or     achat -c [<ip address or hostname>] [<port>]" << endl;
}


int main(int argc, char* argv[]){
    if(argc == 1 || argc > 4){ printHelp(); return -1; }
    
    string address = DEFAULT_ADDRESS;
    string port = DEFAULT_PORT;
    
    auto option = string(argv[1]);
    if(option == "--help" || option == "-h"){
        printHelp();
        return 0;
    }
    
    auto serviceType = option == "-c"?
        achat::ChatService::Type::Client :
        achat::ChatService::Type::Server;
    
    if(ChatService::Type::Client == serviceType){
        if(argc == 3){ address = argv[2];}
        if(argc == 4){ port = argv[3]; }
    }
    else if(ChatService::Type::Server == serviceType){
        if(argc == 3){ port = argv[2]; }
    }
    else { printHelp(); return -1; }

    auto msgHandler = [](Message::Header&, Message::Payload& payload){
        cout << "Them> ";
        string msg(payload.begin(), payload.end());
        cout << msg << endl;
    };

    achat::ChatServiceFactory cf;
    auto chatServicePtr = cf.makeChatService(serviceType,
                                             address,
                                             port,
                                             msgHandler);

    // Set up for threading
    atomic<bool> running{true};

    cout << "Launching network service" << endl;
    auto future = async(launch::async,
                        [&](){chatServicePtr->run();});

    // Get input from the user.
    cout << "Welcome to chat!" << endl;
    while(running){
        string inputText;
        getline(cin, inputText);
        cout << endl;
        if(inputText == "exit"){
            cout << "Exiting..." << endl;
            chatServicePtr->die();
            running = false;
            break;
        }
        chatServicePtr->send(inputText);
    }
    future.wait();
    return 0;
}
