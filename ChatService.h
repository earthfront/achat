#pragma once

#include "Message.h"
#include "Network.h"

#include <boost/bind.hpp>
#include <boost/system/error_code.hpp>
#include <boost/asio.hpp>
#include <boost/asio/read.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/lexical_cast.hpp>

#include <iostream>
#include <chrono>
#include <map>
#include <vector>
#include <string>
#include <tuple>
#include <stdexcept>
#include <mutex>
#include <atomic>
#include <thread>

using namespace std;
using namespace boost;
using namespace asio::ip;
using namespace chrono;
using ClockType = high_resolution_clock;

namespace achat{

    /* 
       Normally I would include a cpp file. 
     */
    class ChatService {
    public:
        using UPtr = std::unique_ptr<ChatService>;
        enum Type {
            Client = 0,
            Server,
            Max
        };

        // Data agnostic func.
        void send(ByteVec& data){
            Message::Header header;
            header.msgType = Message::ChatMessage;
            header.id = messageCounter++;
            header.size = static_cast<uint32_t>(data.size());
            send(header, data);
        }

        // Our clients send strings. 
        void send(string& str){
            auto outData = ByteVec(str.begin(), str.end());
            send(outData);
        }

        // Activate the service. Blocks.
        void run(){
            scheduleRead();
            io_service.run();
        }

        // Kills this and the remote service
        void die(){
            Message::Header header;
            header.msgType = Message::Type::Die;
            send(header);
            io_service.stop();
            running = false;
        }

        // Want to make this private, but make_unique prevents it.
        ChatService(asio::io_service& io_service,
                    unique_ptr<tcp::socket>& socketPtr,
                    Message::ReceiveHandler handler):
            userMessageReceiveHandler(handler),
            io_service(io_service),
            socketPtr(move(socketPtr)),
            sendHeaderBuffer(Message::HeaderSize),
            recvHeaderBuffer(Message::HeaderSize){}
        
    private: // members
        // Housekeeping
        atomic<bool> running{true};
        mutex sendMutex; // See note for below mutex
        
        IdType messageCounter{0};
        Message::ReceiveHandler userMessageReceiveHandler;

        // Networking components
        asio::io_service& io_service;
        unique_ptr<tcp::socket> socketPtr;
        
        // RTT message data structure. Fast random and ordered access
        map<IdType, ClockType::time_point> sendTimes;
        // TODO: In real circumstances, don't protect concurrent access with an internal mutex. That breaks the single responsibility principle. Thread safety should be a separate abstraction. This is used to save time.
        mutex sendTimesMutex;
        
        // Reused network stuff: header, payload buffers
        ByteVec sendHeaderBuffer;
        ByteVec sendPayloadBuffer;

        ByteVec recvHeaderBuffer;
        ByteVec recvPayloadBuffer;

    private: // methods
        static bool isAlphaNumeric(ByteVec data){
            // We're agnostic to data, but print it out if it's a string.
            // ... For this assignment it's a string.
            return true;
        }
        
        void handleMessage(Message::Header& header, Message::Payload& payload){
            switch(header.msgType){
            case Message::Type::Ack:
                {
                    lock_guard<mutex> g(sendTimesMutex);
                    auto now = ClockType::now();
                    auto rtt = now - sendTimes[header.id];
                    
                    // TODO: activate user supplied callback.
                    cout << "Message id: " << header.id
                         << " had round trip time (milliseconds): "
                         << duration_cast<milliseconds>(rtt).count() << endl;
                }
                break;
            case Message::Type::Die:
                cout << "Received message to die." << endl;
                running = false;
                return;
            case Message::Type::ChatMessage:
                userMessageReceiveHandler(header, payload);
                
                // Send ack, retain the id
                header.msgType = Message::Type::Ack;
                header.size = 0;
                send(header);
                break;
            default:
                auto errorMsg = "Unrecognized message type received";
                cout << errorMsg << endl;
                throw runtime_error(errorMsg);
            }
        }

        // Send empty (type/id only) message
        void send(Message::Header& header){
                        lock_guard<mutex> g(sendMutex);
            bytesFromHeader(sendHeaderBuffer, header);
            asio::write(*socketPtr, asio::buffer(sendHeaderBuffer));
        }

        // Send full message
        void send(Message::Header& header, Message::Payload& payload){
                        lock_guard<mutex> g(sendMutex);
            bytesFromHeader(sendHeaderBuffer, header);
            sendPayloadBuffer.assign(payload.begin(), payload.end());
            
            // Write the serialized data to the socket. We use "gather-write" to send both the header and the data in a single write operation.
            vector<asio::const_buffer> buffers;
            buffers.push_back(asio::buffer(sendHeaderBuffer));
            buffers.push_back(asio::buffer(sendPayloadBuffer));
            asio::write(*socketPtr, buffers); // Blocks
            {
                // Record the time sent
                lock_guard<mutex> g(sendTimesMutex);
                sendTimes[header.id] = ClockType::now();
            }
        }
        
    public:
        // Used for the boost::async_read call. Gets control when the read completes.
        void handleReadHeader(const system::error_code& ec,
                              size_t bytesTransferred){
            auto header = headerFromBytes(recvHeaderBuffer);
            //            cout << "Got header. type, size: " << header.msgType << ", " << header.size << endl;
            if( header.size != 0 ){
                recvPayloadBuffer.resize(header.size);
                read(*socketPtr, asio::buffer(recvPayloadBuffer));
            }
            handleMessage(header, recvPayloadBuffer);
            
            // Schedule the next read, else io_service will just return from nothing to do.
            if(running){ scheduleRead(); }
        }
        
    private:
        // Put in for the next read.
        void scheduleRead(){
            async_read(*socketPtr,
                       asio::buffer(recvHeaderBuffer),
                       [&](const system::error_code& ec,
                           size_t bytesTransferred){
                           this->handleReadHeader(ec, bytesTransferred);
                       });
        }
    };

    /* 
       Each ChatService keeps its socket connected for the duration of the chat 
    */
    class ChatServiceFactory{
    public:
        ChatServiceFactory(){}

        // Sockets are the same, bidirectional. Factory pivots on type enum.
        // Lots of boost asio cruft here.
        ChatService::UPtr makeChatService(ChatService::Type type,
                                          string address,
                                          string port,
                                          Message::ReceiveHandler handler){
            auto socketPtr = make_unique<tcp::socket>(io_service);
            if(type == ChatService::Type::Server){
                cout << "Listening for connection on port: "
                     << port
                     << "..." << endl;
                // Set up acceptor, etc.
                acceptorPtr = make_unique<tcp::acceptor>(io_service, tcp::endpoint(tcp::v4(), lexical_cast<uint16_t>(port)));
                system::error_code ec;
                acceptorPtr->accept( *socketPtr, ec);
                if(ec)
                    { throw runtime_error("Failed to create tcp stream"); }
            }
            else{
                tcp::resolver::iterator iterator;
                tcp::resolver::iterator end;
                while(iterator == end){
                    tcp::resolver resolver(io_service);
                    auto targetAddress = address + ":" + port;
                    cout << "Connecting to " << targetAddress << endl;
                    try{
                        tcp::resolver::query query(address, port);
                        iterator = asio::connect(*socketPtr,
                                                 resolver.resolve(query));
                    }
                    /// Terrible practice, done in urgency
                    catch(...){ sleep(1); cout << "Trying again" << endl; }
                }
            }
            
            return make_unique<ChatService>(std::ref(io_service),
                                            std::ref(socketPtr),
                                            handler);
        }
                     
    private:
        asio::io_service io_service;
        unique_ptr<tcp::acceptor> acceptorPtr;
    };
}
