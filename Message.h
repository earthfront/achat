#pragma once

#include <cstdint>
#include <memory>
#include <vector>
#include <functional>

using namespace std;

namespace achat {
    using IdType = uint32_t;
    using Byte = uint8_t;
    using ByteVec = vector<Byte>;

    namespace Message{
        using Payload = ByteVec;
        enum Type : uint8_t {
            ChatMessage=0,
                Ack,
                Die,
                Max
                };
        
        struct Header{
            // Comes over the wire first. Describes the payload to follow.
            Message::Type msgType{Type::ChatMessage};
            IdType id{0};
            uint32_t size{0};
        };
        constexpr auto HeaderSize = sizeof(Header);
        using ReceiveHandler = function<void(Message::Header&,
                                             Message::Payload&)>;
    }        
}
